package castinside.spring.castapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CastapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CastapiApplication.class, args);
	}

}
